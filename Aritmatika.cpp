include <iostream>
using namespace std;

int main() {
    int i, j;
    cout << "Masukkan nilai i: ";
    cin >> i;
    cout << "Masukkan nilai j: ";
    cin >> j;

    int hasil_penambahan = i + j;
    int hasil_pengurangan = i - j;
    int hasil_perkalian = i * j;
    int hasil_pembagian = i / j;
    int hasil_modulo = i % j;

    // Menampilkan tabel operasi dan hasil operasi
    cout << "|--------------------------------|" << endl;
    cout << "|  Operasi    |   Hasil Operasi  |" << endl;
    cout << "|--------------------------------|" << endl;
    cout << "|  " << i << " + " << j << "      |   " << hasil_penambahan << "          \t   |" << endl;
    cout << "|  " << i << " - " << j << "      |   " << hasil_pengurangan << "         \t   |" << endl;
    cout << "|  " << i << " * " << j << "      |   " << hasil_perkalian << "          \t   |" << endl;
    cout << "|  " << i << " div " << j << "    |   " << hasil_pembagian << "          \t   |" << endl;
    cout << "|  " << i << " mod " << j << "    |   " << hasil_modulo << "          \t   |" << endl;
    cout << "|--------------------------------|" << endl;

    return 0;
}
