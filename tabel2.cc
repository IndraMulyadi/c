#include <iostream>
#include <iomanip>

int main() {
    using namespace std;

    int suhuAwal;
    int suhuAkhir;
    int step;

    cout << "Masukkan suhu awal: ";
    cin >> suhuAwal;

    cout << "Masukkan suhu akhir: ";
    cin >> suhuAkhir;

    cout << "Masukkan step: ";
    cin >> step;

    cout << "+--------+------------+-------+" << endl;
    cout << "| Celcius| Fahrenheit | Kelvin|" << endl;
    cout << "+--------+------------+-------+" << endl;

    for (int celsius = suhuAwal; celsius <= suhuAkhir; celsius += step) {
        double fahrenheit = (celsius * 9.0 / 5.0) + 32;
        double kelvin = celsius + 273.15;
        cout << "| " << setw(6) << celsius << " | " << setw(10) << fahrenheit << " | " << setw(5) << kelvin << " |" << endl;
    }

    cout << "+--------+------------+-------+" << endl;

    return 0;
}
